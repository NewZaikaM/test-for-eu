import React from 'react';
import Container from '@mui/material/Container';
import Header from './../componetns/Header/Header';
import Content from './../componetns/Content/Content';

const PageShowingPokemon = () => {
	return (
		<Container maxWidth="lg" sx={{ padding: '0 150px', }}>
			<Header />
			<Content />
		</Container>
	);
};

export default PageShowingPokemon;
