import axios from 'axios';

import { urlPokemons } from './setApi';
import { TYPES_STATE_DATA } from '../utils/constants';
import { utils } from '../utils/utils';

export async function getPokemons(setState, url) {
	try {
		setState((prevData) => {
			return {
				...prevData,
				stateData: TYPES_STATE_DATA.PENDING,
			};
		});

		const response = await axios.get(url);
		utils.checkResponseServer(response);

		if (url === urlPokemons) {
			const setPokemons = response.data.results;

			setState((prevData) => {
				return {
					...prevData,
					setPokemons,
				};
			});

			const urlShowingPokemon = response.data.results[0].url;
			getPokemons(setState, urlShowingPokemon);
		} else {
			const showingPokemon = response.data;
			setState((prevData) => {
				return {
					...prevData,
					showingPokemon,
					stateData: TYPES_STATE_DATA.GOT,
				};
			});
		}
	} catch (error) {
		setState((prevData) => {
			return {
				...prevData,
				stateData: TYPES_STATE_DATA.ERROR,
				objectError: error,
			};
		});
	}
}
