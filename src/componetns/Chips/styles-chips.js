const stylesChips = {
	externalBox: {
		width: '50%',
		display: 'flex',
		rowGap: '10px',
		columnGap: '6px',
		justifyContent: 'flex-start',
		alignItems: 'center',
		flexWrap: 'wrap',
		fontFamily: 'inherit',
		fontSize: '20px',
		fontWeight: 'medium',
	},
	chip: {
		padding: '30px 20px',
		borderRadius: '44px',
		fontFamily: 'inherit',
		fontSize: 'inherit',
		fontWeight: 'inherit',
		background: '#1986ec',
		'& .MuiChip-label': {
			padding: 0,
		},
		'&:hover': {
			background:
				'linear-gradient(0deg, rgba(0, 0, 0, 0.20) 0%, rgba(0, 0, 0, 0.20) 100%), #1986EC',
		},
		'&:disabled': {
			background: 'rgba(25, 134, 236, 0.4)',
		},
		'&:click': {
			background: '#1986ec',
		},
	},
};

export default stylesChips;
