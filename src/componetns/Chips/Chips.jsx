import React from 'react';
import { Box, Chip } from '@mui/material';

import stylesChips from './styles-chips';
import { utils } from '../../utils/utils';

const Chips = ({ setPokemons, changeShowingPokemon }) => {
	return (
		<Box sx={stylesChips.externalBox}>
			{setPokemons.map((pokemon) => (
				<Chip
					component="button"
					sx={stylesChips.chip}
					label={utils.сonvertСapitalizedWord(pokemon.name)}
					clickable
					onClick={(event) => {
						event.stopPropagation();
						event.preventDefault();
						changeShowingPokemon(pokemon);
					}}
					key={pokemon.name}
				/>
			))}
		</Box>
	);
};
export default Chips;
