const stylesLinkToApi = {
  link: {
		color: '#fff',
		padding: '7px',
		border: '1px #fff solid',
		fontFamily: 'inherit',
		fontSize: '12px',
		lineHeight: '112.7%',
		fontWeight: '500',
		textTransform: 'uppercase',
	},
};

export default stylesLinkToApi