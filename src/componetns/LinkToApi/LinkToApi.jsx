import React from 'react';
import { Link } from '@mui/material';

import stylesLinkToApi from './styles-link-to-api';

const LinkToApi = () => {
	return (
		<Link href="https://pokeapi.co/" underline="none" sx={stylesLinkToApi.link}>
			Покемоны API
		</Link>
	);
};

export default LinkToApi;
