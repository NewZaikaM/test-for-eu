import React from 'react';
import { Box } from '@mui/material';

import TrumpetСall from '../trumpetСall/trumpetСall';

import stylesHeader from './styles-header';
import LinkToApi from '../LinkToApi/LinkToApi';

const Header = () => {
	return (
		<Box component="header" sx={stylesHeader.wrapper}>
			<LinkToApi />
			<TrumpetСall />
		</Box>
	);
};

export default Header;
