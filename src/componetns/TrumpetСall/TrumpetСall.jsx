import { Box, Typography } from '@mui/material';
import React from 'react';

import icon from "./../../assets/icon.svg"

import stylesTrumpetСall from './styles-trumpet-call';

const TrumpetСall = () => {
	return (
		<Box sx={stylesTrumpetСall.wrapper}>
			<img src={icon} alt="icon-click" />
			<Typography sx={stylesTrumpetСall.text}>
				Нажмите на
				<br />
				нужное Покемона
			</Typography>
		</Box>
	);
};

export default TrumpetСall;
