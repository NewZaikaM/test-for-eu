const stylesTrumpetСall = {
	wrapper: { display: 'flex', alignItems: 'center', gap: '10px' },
	text: {
		fontFamily: 'inherit',
		fontSize: '12px',
		lineHeight: '100%',
		fontWeight: '600',
		textAlign: 'left',
	},
};

export default stylesTrumpetСall