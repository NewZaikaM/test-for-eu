import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';

import stylesCard from './styles-card.js';

import { utils } from '../../utils/utils.js';
import { Fade, Zoom } from '@mui/material';

const PokeCard = ({ showingPokemon }) => {
	const {
		name,
		id,
		height,
		moves,
		stats,
		sprites: { front_shiny: pathImg },
	} = showingPokemon;
	const capitalizedName = utils.сonvertСapitalizedWord(name);
	const correctNumberSeries = utils.getCorrectNumberSeries(moves);
	const attack = utils.getAttack(stats);

	return (
		<Card sx={stylesCard.card}>
			<Typography component="h2" sx={stylesCard.title}>
				{capitalizedName}
			</Typography>
			<Zoom timeout={1000} in>
				<CardMedia
					component="img"
					height="200"
					image={`${pathImg}`}
					alt={`pokemon ${capitalizedName}`}
					sx={{
						objectFit: 'contain',
					}}
				/>
			</Zoom>
			<CardContent sx={stylesCard.cardContent}>
				<Typography sx={stylesCard.fieldText}>{correctNumberSeries}</Typography>
				<Typography sx={stylesCard.fieldText}>Id: {id}</Typography>
				<Typography sx={stylesCard.fieldText}>Height: {height}</Typography>
				<Typography sx={stylesCard.fieldText}>Attack: {attack}</Typography>
			</CardContent>
		</Card>
	);
};

export default PokeCard;
