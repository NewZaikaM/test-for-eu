const stylesCard = {
	card: {
		width: "50%",
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'flex-start',
		gap: '44px',
		padding: '44px 44px 16px 44px',
		backgroundColor: '#000',
		fontFamily: 'Raleway',
		color: '#a0a0a0',
		height: '500px',
	},
	title: {
		fontFamily: 'inherit',
		color: 'inherit',
		fontSize: '48px',
		fontWeight: 'bold',
		lineHeight: '100%',
	},
	cardContent: {
		display: 'flex',
		padding: '0',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'flex-start',
		fontFamily: 'inherit',
		color: 'inherit',
		fontSize: '17px',
		fontWeight: 'medium',
		lineHeight: '150%',
		"&:last-child": {
			paddingBottom: 0
		}
	},
	fieldText: { color: 'inherit', fontFamily: 'inherit' },
};

export default stylesCard