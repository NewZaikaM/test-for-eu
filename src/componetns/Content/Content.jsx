import React, { useEffect, useState } from 'react';

import SuccessfulContent from './SuccessfulContent';
import ErrorContent from './ErrorContent';
// import Loader from '../Loader/Loader';

import { TYPES_STATE_DATA } from '../../utils/constants';
import { getPokemons } from '../../api/setRequests';
import { urlPokemons } from '../../api/setApi';

const Content = () => {
	const [data, setData] = useState({
		setPokemons: [],
		showingPokemon: null,
		stateData: TYPES_STATE_DATA.EMPTY,
		objectError: null,
	});
	useEffect(() => {
		getPokemons(setData, urlPokemons);
	}, []);

	const changeShowingPokemon = (pokemon) => {
		getPokemons(setData, pokemon.url);
	};

	const { setPokemons, showingPokemon, objectError: error, stateData } = data;

	let content = <></>;

	if (stateData === TYPES_STATE_DATA.ERROR) {
		content = <ErrorContent error={error} />;
	}

	if (stateData === TYPES_STATE_DATA.GOT) {
		content = (
			<SuccessfulContent
				setPokemons={setPokemons}
				showingPokemon={showingPokemon}
				changeShowingPokemon={changeShowingPokemon}
			/>
		);
	}

	return content;
};

export default Content;
