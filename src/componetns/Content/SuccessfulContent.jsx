import React from 'react';
import { Box } from '@mui/material';

import Chips from '../Chips/Chips';
import PokeCard from '../Card/Card';

import stylesContent from './styles-content';

const SuccessfulContent = ({
	setPokemons,
	changeShowingPokemon,
	showingPokemon,
}) => {
	return (
			<Box sx={stylesContent}>
				<Chips
					setPokemons={setPokemons}
					changeShowingPokemon={changeShowingPokemon}
				/>
				<PokeCard showingPokemon={showingPokemon} />
			</Box>
	);
};

export default SuccessfulContent;
