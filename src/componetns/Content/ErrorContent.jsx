import React from 'react';
import { Alert } from '@mui/material';

const ErrorContent = ({ error }) => {
	return (
		<Alert severity="error" sx={{ backgroundColor: '#000' }}>
			{error.message}
		</Alert>
	);
};

export default ErrorContent;
