const сonvertСapitalizedWord = (word) => word[0].toUpperCase() + word.slice(1);
const getCorrectNumberSeries = (series) => {
	const numberSeries = series.length;
  if(!numberSeries) {
    return "Не снялся ни в одной серии"
  }
  if(numberSeries % 10 === 1) {
    return `Снялся в ${numberSeries} серии`
  }

  return `Снялся в ${numberSeries} сериях`
};
const getAttack = (stats) => {
	let attack = 'unknown';
	for (let stat of stats) {
		if (stat.stat.name === 'attack') {
			attack = stat.base_stat;
		}
	}
	return attack;
};
const checkResponseServer = (response) => {
	if (response.status !== 200) {
		throw new Error(
			`Sorry, we have problems, we'll try to resolve in the near future`,
		);
	}
}


export const utils = {
	сonvertСapitalizedWord,
  getCorrectNumberSeries,
	getAttack,
	checkResponseServer
};
