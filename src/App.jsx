import React from 'react';
import PageShowingPokemon from './pages/PageShowingPokemon';

function App() {
	return (
			<PageShowingPokemon />
	);
}

export default App;
